﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsProjekt.Models;

namespace WebFormsProjekt
{
    public partial class SiteMaster : MasterPage
    {

        public string username;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current != null && HttpContext.Current.Session != null)
            {
                var user = HttpContext.Current.Session["user"] as User;
                if (user != null)
                {
                    username = user.Username;
                } else
                {
                    username = "Nobody";
                }
            }

        }
    }
}