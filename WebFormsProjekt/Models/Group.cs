﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFormsProjekt.Models
{
    public class Group
    {
        public int Id { get; set; }
        public int Id_prof { get; set; }     // pod kojim je profesorom grupa
        public string Name { get; set; }     //  ime grupe npr. oip1
        public int Course { get; set; }      // smjer (racunarstvo, mehatronika, sestrinstvo)
        public int Year { get; set; }        // koja godina (1, 2, 3)
        public string Subject { get; set; }  // predmet (kolegij) npr. matematika
    }
}