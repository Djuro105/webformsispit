﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebFormsProjekt.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Authority { get; set; }     // sto je user (profesor, student, admin)
    }
}