﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsProjekt.Models;
using WebFormsProjekt.Persistence;
using WebFormsProjekt.Persistence.Abstractions;

namespace WebFormsProjekt
{
    public partial class _Default : Page
    {
        private IGroupRepository _groupRepository;
        protected int id_prof;

        protected void Page_Load(object sender, EventArgs e)
        {
            

            var user = HttpContext.Current.Session["user"] as User;
            if (user != null)
            {
                id_prof = user.Id;

                _groupRepository = new SqlLiteGroupDapperRepository();
                var groups = _groupRepository.GetAllGroupsOfProf(id_prof);



                // Učitaj inicijalne podatke u grid samo kod prvog učitavanja stranice
                if (!IsPostBack)
                {
                    //ResetForm();
                    GetGroupsData();
                }
            }

        }

        private void btnLogout_Click()
        {
            var group = new Group
            {
              
            };
        }

        protected void GetGroupsData()
        {
            var groups = _groupRepository.GetAllGroupsOfProf(id_prof);

            gvGroups.DataSource = groups;
            gvGroups.DataBind();
        }
    }
}