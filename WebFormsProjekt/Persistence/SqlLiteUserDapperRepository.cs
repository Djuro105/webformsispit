﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebFormsProjekt.Models;
using WebFormsProjekt.Persistence.Abstractions;

namespace WebFormsProjekt.Persistence
{
    public class SqlLiteUserDapperRepository : SqlLiteBaseRepository, IUserRepository
    {
        public void CreateUser(User user)
        {
            throw new NotImplementedException();
        }

        public List<User> GetAllUsers()
        {
            throw new NotImplementedException();
        }

        public User GetUserById(int userId)
        {
            throw new NotImplementedException();
        }

        public User Login(string email, string password)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                var user = connection.QueryFirstOrDefault<User>("SELECT id, username, firstName, authority lastName " +
                    "FROM users where email = @email AND " +
                    "password = @password",
                    new { email = email, password = password });

                return user;
            }
        }

        public void UpdateUser(User user)
        {
            throw new NotImplementedException();
        }
    }
}