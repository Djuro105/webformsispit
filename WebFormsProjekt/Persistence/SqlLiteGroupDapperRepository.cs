﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebFormsProjekt.Models;
using WebFormsProjekt.Persistence.Abstractions;

namespace WebFormsProjekt.Persistence
{
    public class SqlLiteGroupDapperRepository : SqlLiteBaseRepository, IGroupRepository
    {
        public void CreateGroup(Group group)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
                connection.Execute("INSERT INTO groups(id_prof, name, course, year, subject) values(@id_prof, @name, @course, @year, @subject)",
                    new
                    {
                        id_prof = group.Id_prof,
                        name = group.Name,
                        course = group.Course,
                        year = group.Year,
                        subject = group.Subject
                    });

            }
        }

        public void DeleteGroup(Group group)
        {
            throw new NotImplementedException();
        }

        public List<Group> GetAllGroupsOfProf(int Id_prof)
        {
            using (var connection = GetDbConnection())
            {
                connection.Open();
               var groups = connection.Query<Group>("SELECT id, name, course, year, subject FROM groups where id_prof = @id",
                   new { id = Id_prof });

                return groups.ToList();
            }
        }

        public Group GetGroupById(int groupId)
        {
            throw new NotImplementedException();
        }
    }
}