﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebFormsProjekt.Models;

namespace WebFormsProjekt.Persistence.Abstractions
{
    interface IGroupRepository
    {
        List<Group> GetAllGroupsOfProf(int Id_prof);
        Group GetGroupById(int groupId);
        void CreateGroup(Group group);
        void DeleteGroup(Group group);
    }
}
