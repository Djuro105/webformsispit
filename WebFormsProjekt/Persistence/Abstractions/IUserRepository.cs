﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebFormsProjekt.Models;

namespace WebFormsProjekt.Persistence.Abstractions
{
    interface IUserRepository
    {
        User Login(string email, string password);
        User GetUserById(int userId);
        List<User> GetAllUsers();

        void UpdateUser(User user);
        void CreateUser(User user);
    }
}
