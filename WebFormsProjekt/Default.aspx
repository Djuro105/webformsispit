﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebFormsProjekt._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <div class="jumbotron">
        <h1>VUB izostanci</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
    </div>



   <asp:GridView ID="gvGroups" CssClass="table" runat="server" AutoGenerateColumns="false">
        <Columns>           
            <asp:TemplateField>
                <ItemTemplate>
                    <a href="#">
                        <div style="background: gray;">
                            <div><%# DataBinder.Eval(Container.DataItem, "id") %> <%# DataBinder.Eval(Container.DataItem, "name") %> <%# DataBinder.Eval(Container.DataItem, "subject") %></div>
                            
                        </div>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>




    <!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3 class="modal-title" id="exampleModalLabel">New Group</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <label>Name</label>
          <input type="text" name="name" value="" /> <br />
          <label>Course</label>
          <input type="number" name="name" value="" /> <br />
          <label>Year</label>
          <input type="number" name="name" value="" /> <br />
          <label>Subject</label>
          <input type="text" name="name" value="" /> <br />
          <input type="number"  name="name" value="<%=id_prof%>" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>

    <!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Add New Group
</button>


</asp:Content>
