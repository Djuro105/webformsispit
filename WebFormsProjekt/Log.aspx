﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Log.aspx.cs" Inherits="WebFormsProjekt.Log" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Login stranica</h1>

    <div class="form-group">
        <label for="txtEmail">Username</label>
        <asp:TextBox ID="txtEmail" CssClass="form-control" runat="server"></asp:TextBox>
    </div>
    <div class="form-group">
        <label for="txtPassword">Password</label>
        <asp:TextBox ID="txtPassword" CssClass="form-control" TextMode="Password" runat="server"></asp:TextBox>
    </div>

    <asp:Button runat="server" ID="btnLogin" CssClass="btn btn-primary" Text="Login" OnClick="btnLogin_Click" />

    <asp:Panel runat="server" ID="pnlPoruke" Visible="false">
    <asp:Label runat="server" ID="lblPoruka"></asp:Label>
    </asp:Panel>



</asp:Content>

